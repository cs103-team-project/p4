/* CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <string>
#include <set>
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iomanip>
#include <fstream>
#include <iostream>
using std::string;
using std::set;
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

enum STATES {s,ns}; // space and not space

size_t cb (int a) { //count bytes
	a++;
	return a;
}
size_t cw (int b, STATES &state, const char x){ //count word
	if (state == ns){
		if (x== ' ' || x == '\t' || x == '\n'){
			b++;
		}
	}
	return b;
}
size_t cl (int c, const char x){ //count lines
	if (x=='\n'){
		c++;
		return c;
	}
	return c;
}
size_t cL (int d, int e, const char x){ //count length
	if (x=='\n'){
		if (e > d){
			d = e;
		}
		e = 0;
	}
		else if (x=='\t'){
			e+=8-e%8;
		}
		else
			e++;
		return d;
	}
set<string> fu (set<string> uword, string &str, STATES &state, const char x){ //found the uniq
	if (state == ns){
		if (x==' ' || x=='\t' || x=='\n'){
			uword.insert(str);
			str.clear();
			state = s;
		}
		else
			str+=x;
	}
	else
		if (x!=' ' || x!='\t' || x!='\n'){
			str+=x;
			state = ns;
		}
	return uword;
	}
int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				//charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	int bytes = 0;
	int words = 0;
	int lines = 0;
	int maxL = 0;
	int length = 0;
	//bool in_word = false;
	char x;
	if (ch == ' '){
		bytes++;
		length++;
	}
	else if (ch == '\t'){
		state = s;
		bytes++;
		length += 8-length%8;
	}
	else if (ch == '\n'){
		state =s;
		bytes++;
		lines++;
	}
	else{
		state = ns;
		bytes++;
		str +=ch;
		length++;
	}
	set <string> uword;
	while (fread(&x,1,1,stdin)){
		cb(bytes);
		cl(lines,x);
		cw(words,state,x);
		cu(uword,str,state,x);
		cL(maxL,length,x);
	}
	if (feof(stdin)){
		if (state == ns){
			u.insert(str);
			words++;

	/*while (x != EOF){
		bytes++;
		switch (c) {
			case '/n':
				lines++;
			case ' ':
			case '/t':
				in_word = false;
				break;
			default:
				if (! in_word){
					in_word =true;
					words++;
				}
				break;
			}
		}*/
	}

	if (charonly == 1){
		cout << bytes <<endl;
}
	if (linesonly == 1){
		cout << lines << endl;
}
	if (wordsonly == 1){
		cout << words << endl;
}
	if (uwordsonly == 1){
		cout << uword << endl;
}
	if (longonly == 1){
		cout << maxL << endl;
}
	return 0;
}
}

