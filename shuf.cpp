/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
*Vector of structs initialization:
https://stackoverflow.com/questions/8067338/vector-of-structs-initialization
*
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 2+2+4
 */

#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

//struct that handles different types for a vector
struct vectype{
	string vecstr;
	int vecint;
};

	//function that converts vector array to string
void arr_to_str(vector<char>& A, string& B){
		B = ""; //set B to empty string
	for(size_t i = 0; i<A.size();i++){
		B = B + A[i]; //put every element of array A into string B;
	}
}

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */

	//a vector of structure that handles the target input of different type
	vector<vectype> T;


/*********************************************
 * if user supplies -e, stores all the
 * strings from the user input in the
 * argument, shuffle and print the whole vector
 * or certain number of lines when -n is
 * supplied
	*********************************************/
	if(echo == 1){
		int p = 0; //index for the vector T;
		while (optind < argc){
			T.push_back(vectype());
			T[p++].vecstr = argv[optind++];
		}

	//do shuffle: generate a randon number and
	//store it in randnum, then swap the first
	//element of the vector with the random element,
	//and next swap the second element with the random
	//element from 2 - size of the vector, go on until the vector runs out.

		int randnum = 0; //store the random number
		srand(time(0));
		for(size_t i = 0; i<T.size(); i++){
			size_t vsize = T.size();
			vsize -= i;
			randnum = rand() % vsize;
			randnum += i;
			swap(T[i].vecstr,T[randnum].vecstr);
		}

  //	print out the shuffled vector;
  //  need to check that:
  //  whether user supplies -n and
  //	whether	the option target after -n is
  //	greater than the size of our vector to avoid seg falut.*/

	int n = count;
	int m = T.size();
	if(n != -1 && n	<= m){
			for(int i = 0; i < n; i++){
				cout<<T[i].vecstr<<"\n";
			}
		}
		else{
			for(size_t i = 0; i < T.size(); i++){
				cout<<T[i].vecstr<<"\n";
			}
		}
	}


/*********************************************
 * if user supplies -i, stores all the
 * integers from rlow to rhigh in the
 * vector, shuffle and print the whole vector
 * or certain number of lines when -n is
 * supplied
*********************************************/

	else if(userange == true){
		int p = 0; //index for the vector T;
		for(int i = rlow; i<=rhigh; i++){
			T.push_back(vectype());
			T[p++].vecint = i;
			}

//-----shuffle--------
		int randnum = 0;
		srand(time(0));
		for(size_t i = 0; i<T.size(); i++){
			size_t vsize = T.size();
			vsize -= i;
			randnum = rand() % vsize;
			randnum += i;
			swap(T[i].vecint,T[randnum].vecint);
		}

//------print---------
		int n = count;
		int m = T.size();
		if(n != -1 && n	<= m){
			for(int i = 0; i < n; i++){
				cout<<T[i].vecint<<"\n";
			}
		}
		else{
			for(size_t i = 0; i < T.size(); i++){
				cout<<T[i].vecint<<"\n";
			}
		}
	}

/**********************************************
 * if none of -e or -i is used,read from stdin:
 * read one byte a time from stdin, first
 * store it in an vector array, then convert the
 * array to a string, finally store the string
 * in the vector of strings
**********************************************/

	else{
	char c; 					//store the byte read from stdin;
	int p = 0; 				//index for the vector T;
	vector<char> V; 	//array that stores each lines of the stdin;
	string s; 				//convert vector array V to string s;

	//read bytes from stdin
	while	((fread(&c,1,1,stdin)) != 0){
		if(c == '\n'){
	//function that converts the array to string
		arr_to_str(V,s);
	//store the string in the vector string T
		T.push_back(vectype());
		T[p++].vecstr	=	s;
		V.clear();
		}
		else{
  //store the byte read from stdin to the array V
			V.push_back(c);
		}
	}
	//note that the after the while ends, the last
	//line of the stdin has been read but not
	//stored yet(since the last '\n' is in the end
	//of the second line from the bottom)
	//so we need to add it to the vector T;

	arr_to_str(V,s);
	T.push_back(vectype());
	T[p++].vecstr	=	s;


//-----shuffle -----
	int randnum = 0;
		srand(time(0));
		for(size_t i = 0; i<T.size(); i++){
			size_t vsize = T.size();
			vsize -= i;
			randnum = rand() % vsize;
			randnum += i;
			swap(T[i].vecstr,T[randnum].vecstr);
		}

//------print---------
		int n = count;
		int m = T.size();
		if(n != -1 && n	<= m){
			for(int i = 0; i < n; i++){
				cout<<T[i].vecstr<<"\n";
			}
		}
		else{
			for(size_t i = 0; i < T.size(); i++){
				cout<<T[i].vecstr<<"\n";
			}
		}

	}

	return 0;
}

