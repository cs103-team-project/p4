/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: .. write me.*/
	string ss;
	size_t i = 0;
	while(i<s.length()){

		if(s[i] == '\\' && s[i+1] == '\\'){
			i++;
		}
		else if(s[i] == '\\' && s[i+1] == 'n'){
			ss.push_back('\n');
			i += 2;
		}
		else if(s[i] == '\\' && s[i+1] == 't'){
			ss.push_back('\t');
			i += 2;
		}
		else if(s[i] == '\\' && s[i+1] != 'n' && s[i+1] != 't' && s[i+1] != '\\'){
			ss.push_back(s[i+1]);
			i += 2;
		}
		else{
			ss.push_back(s[i]);
			i++;
		}
	}
		s = "";
		for(size_t i = 0; i<ss.length(); i++){
			s.push_back(ss[i]);
		}
}




int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);


	/* TODO: finish this... */

	char a; //stores the input from stdin;

	
/* Complement (-c)
	It keeps the elements same to set 1 and translate  all the others respect to set 2;
*/
	if(comp == 1 && del == 0){
		vector<char> V;//vector that stores the translation of each input character;
	
	//first makeup the table with all element in set 2 and fill the rest up to 256 with the last elemnt of set 2
		for(size_t i = 0; i < s2.length();i++){
			V.push_back(s2[i]);
	}
		size_t k = s2.length()-1;
		for(size_t j = s2.length();j<256;j++){
			V.push_back(s2[k]);
		}

		while	((fread(&a,1,1,stdin)) != 0){
		int count;
		for(size_t i = 0; s1[i] != '\0';i++){
			if(a == s1[i]){
				count = 1;
				break;
			}
		}
 // print out the input if it is in set 1
		if(count != 1){
			cout<<a;
		}
// count for the holes of the difference in their indexs
// and print the correpsonding value in the table;
		else{
			int holes = 0;
			for(size_t i = 0; i<s1.length(); i++){
				if((int)s1[i] < (int)a)
					holes++;
			}
			cout<<V[(int)a - holes];
		}
		}
	}

/* Delete 
	It delets the elements in set 1 when only -d is supplied; delete whatever is not in set 1 when -c is supplied;
*/

	if(del == 1){
	// with -c : print whatever is in set 1;
		if(comp == 0){
			while(fread(&a,1,1,stdin)){
				int count = 0;
				for(size_t i = 0; i<s1.length();i++){
					if(a == s1[i]){
						count = 1;
						break;
					}
				}
				if(count != 1){
					cout<<a;
				}
			}
		}
	//without -c: print whatever is not in set 1;
		else{
			while(fread(&a,1,1,stdin)){
				int count = 0;
				for(size_t i = 0; i<s1.length(); i++){
					if(a == s1[i]){
						count = 1;
						break;
					}
				}
				if(count == 1){
					cout<<a;
				}
			}
		}
	}

/* 
	Translate (witout any option)
	It modifies the input by changing the elements in set 1 with set 2;
*/
	if(comp == 0 && del == 0){

			vector<char> T;

/* If the the length of set 1 is greater than set 2, notice that the extra part of set 1 will	be transferred to the last element of set 2 */
			if(s1.length() > s2.length()){
				for(size_t i = 0; i < s2.length(); i++){
					T.push_back(s2[i]);
				}
				size_t k = s2.length()-1;
				for(size_t j = k; j<s1.length();j++){
					T.push_back(s2[k]);
				}
			}
//If the length of set 1 is less than set2, just follow up with set 1;
			else{
				for(size_t i = 0; i < s1.length();i++){
					T.push_back(s2[i]);
				}
			}

			while(fread(&a,1,1,stdin)){
				int count = 0;
				size_t i;
				for(i = 0; i < s1.length(); i++){
					if(a == s1[i]){
						count = 1;
						break;
					}
				}
				if(count == 1){
				cout<<T[i];
				}
				else{
				cout<<a;
				}
			}
		}


	return 0;
}

