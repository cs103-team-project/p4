#include <cstdio>
#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	if (showcount != 1 && dupsonly != 1 && uniqonly != 1) {
		string x = "_____"; // most recent element we've seen.
		string str;
		while (getline(cin, str)){
			if (x != str){
				x = str;
				cout << str << endl;
			}
		}
	}

	if (showcount == 1 && dupsonly != 1 && uniqonly != 1){
		size_t count = 0;
		string x = "_____";
		string str;
		while (getline(cin,str)){
			if (x == "_____"){
				x = str;
			}
			if (x == str){
				count++;
			}
			else if (x != str) {
				printf("%7lu ",count);
				cout << x << endl;
				count = 1;
			}
			x = str;
		}
		if (count > 0){
				printf("%7lu ",count);
				cout << x << endl;
			}
	}

	if (dupsonly == 1 && showcount != 1 && uniqonly != 1){
		size_t count = 0;
		string x = "_____";
		string str;
		while (getline(cin,str)){
			if (x == "_____"){
				x = str;
			}
			if (x == str){
				count++;
			}
			else if (x != str) {
				if (count > 1){
				  cout << x << endl;
				  count = 1;
				}
			}
			x = str;
		}
		if (count > 1){
				cout << x << endl;
		}
	}

	if (uniqonly == 1 && showcount != 1 && dupsonly != 1){
		size_t count = 0;
		string x = "_____";
		string str;
		while (getline(cin,str)){
			if (x == "_____"){
				x = str;
			}
			if (x == str){
				count++;
			}
			else if (x != str) {
				if (count == 1){
				  cout << x << endl;
					count = 1;
				}
				if (count != 1){
					count = 1;
				}
			}
			x = str;
		}
		if (count == 1){
				cout << x << endl;
			}
	}

	if (uniqonly == 1 && showcount == 1 && dupsonly != 1){
		size_t count = 0;
		string x = "_____";
		string str;
		while (getline(cin,str)){
			if (x == "_____"){
				x = str;
			}
			if (x == str){
				count++;
			}
			else if (x != str) {
				if (count == 1){
					printf("%7lu ",count);
				  cout << x << endl;
					count = 1;
				}
				if (count != 1){
					count = 1;
				}
			}
			x = str;
		}
		if (count == 1){
				printf("%7lu ",count);
				cout << x << endl;
			}
	}

	if (dupsonly == 1 && showcount == 1 && uniqonly != 1){
		size_t count = 0;
		string x = "_____";
		string str;
		while (getline(cin,str)){
			if (x == "_____"){
				x = str;
			}
			if (x == str){
				count++;
			}
			else if (x != str) {
				if (count > 1){
					printf("%7lu ",count);
				  cout << x << endl;
				  count = 1;
				}
			}
			x = str;
		}
		if (count > 1){
				printf("%7lu ",count);
				cout << x << endl;
			}
	}
	return 0;
}
