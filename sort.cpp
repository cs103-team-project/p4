#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
#include <vector>
#include <algorithm>
#include <Bits.h>
#include <iterator>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse    	Sort descending.\n"
"   -f,--ignore-case	Ignore case.\n"
"   -u,--unique     	Don't output duplicate lines.\n"
"   --help          	Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
    bool operator()(const string& s1, const string& s2) {
   	 return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
    }
};
/* How does this help?  Well, if you declare
 *	set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */

bool comp(string a, string b) {
	return a > b;
}

vector<string> descendsort(vector<string> tem) {
	int x = tem.size();
	sort(tem.begin(), tem.end(), comp);
	return tem;
}
/*two void functions above are degined to sort the vecotr of stirng decending...*/ 


void print(map<string, int>f) {
	for (auto pair : f) {
		if (pair.second == 1) {
			cout << pair.first << "\t" << pair.second << endl;
		}}}
/*this function is degined to print the unique*/

void ignorecase(set<string> ign){
    for (auto v : ign){
            cout << v << endl;}
}

void vdisplay(vector<string> tem1) {
	printf("printing vector......\n");
		for (size_t i = 0; i < tem1.size(); i++) {
		cout << tem1[i] << endl;
	}
}
/*this function is degined to print the vector*/



int main(int argc, char *argv[]) {
    // define long options
    static int descending=0, ignorecase=0, unique=0;
    static struct option long_opts[] = {
   	 {"reverse",   	no_argument,   0, 'r'},
   	 {"ignore-case",   no_argument,   0, 'f'},
   	 {"unique",    	no_argument,   0, 'u'},
   	 {"help",      	no_argument,   0, 'h'},
   	 {0,0,0,0}
    };
    // process options:
    char c;
    int opt_index = 0;
    while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
   	 switch (c) {
   		 case 'r':
   			 descending = 1;
   			 break;
   		 case 'f':
   			 ignorecase = 1;
   			 break;
   		 case 'u':
   			 unique = 1;
   			 break;
   		 case 'h':
   			 printf(usage,argv[0]);
   			 return 0;
   		 case '?':
   			 printf(usage,argv[0]);
   			 return 1;
   	 }
 	
	cout << "you have entered " << argc << "arguments." << "\n";
    
	map<string, int> f;
	vector<string> word, sortword;
	set<string> ign;

    /*read from std::in and stored them into vector*/
	for(int i = optind; i < argc; ++i){
		word.push_back(argv[i]);
        /*or I can also add
        f[argv[i]]++;
		ign.insert(argv[i]);*/
	}
    
    /*sort the vector*/
    sortword = descendsort(word);
    
    /*wrtie all strings in the vector into map and set*/
    for (string o : sortword){
    f[o]++;
    ign.insert(o);}
    
    cout << "sort the input decending...";
    	for (auto z : sortword) {
		cout << z << "\n";}
    }
    
    if(ignorecase == 1){
    cout << "ignore the case."
    ignorecase(ign);}
    
    if(unique == 1){
    cout << "printing unique...";
    print(f);
    }   
    }